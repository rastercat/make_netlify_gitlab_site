# Creating a static site on Netlify and auto-deploy from GitLab

This is an example and how-to for hosting a static site on Netlify with automatic deploy from GitLab. 

## 1. Sign up for a Netlify account.

They have what is basically a free tier that allows you to publish a single site on a *.site.netlify.app subdomain. If you want a custom domain or additional sites then you must pay for those options.

You can choose to authenticate for your Netlfiy account with your GitHub or GitLab or separately via an email.

<https://app.netlify.com/signup>
 When you get to the "Deploy your first project" page select "Deploy with GitLab".

You will need to authenticate into GitLab and authorize Netlify.

## 2. Choose or create a repo from GitLab.

You will be asked to choose a GitLab repository.

If you don't have a project leave this window open and got create one now at GitLab before returning.

You can use the example content in the content/ folder of this repo as a starting point.

Select the repo you want to deploy on the Netlify page (after creating it if needed).

## 3. Fill in the form options for the repository you selected.

- Make sure the  correct branch is selected, typically main or master.
- Note that for this repo we need to set the base directory and publish directory to "content/".
- For your own, it should be whatever directory you have the static content in. If you use a static site content generator it will need to match the directory where it outputs the content.

## 4. Click the "Deploy to Netlify" button.

This will deploy your site initially. We will automate this in later steps.

On the "Deploy Success" page note the site name for later.

For example, mine was "vocal-croissant-4b1a20".

After the initial deployment, click "Get Started"

## 5. Examine your initial web site.

- Click "Sites"
- Click on your new site
- Click "Site overview"
- Find the site's URL under "Site Overview".

## 6. OPTIONAL: Local setup

If you want to deploy manually from your local machine you can install netlify-cli locally to test your deployment. If you already have node and npm installed you can install it like this:

You can visit <https://nodejs.org/en/learn/getting-started/how-to-install-nodejs> for instructions on how to install NodeJS and NPM first if needed.

```bash
npm i netlify-cli -g
```

## 7. Get an access token for Netlify.

- You will need this for the GitLab CI/CD deployment process to authenticate to your Netlify account.
- You can get this by visiting <https://app.netlify.com/user/applications>
- NOTE: Save the token, you will not be able to see it again.

## 8. Go to your GitLab project and click Settings.

- Go back to your GitLab project.
- In the left navigation settings, click "Settings" then "CI/CD"
- Expand the variables section
- Add two variables, one each for:
  - NETLIFY_AUTH_TOKEN - this should be the access token you generated in the last step.
  - NETLIFY_SITE_ID - This is your site ID, it is a big UUID (letters and numbers) listed in "Site Information" section of Site details on your Site Configuration page on netlify.

## 9. Set up a custom subdomain at Netlify.

- Got back to your site configuration at Netlify
  - Go back to https://app.netlify.com/
  - Click sites
  - Click on your new site
  - Click "Site configuration" in left navigation area
- Click "Change site name" button in the "Site information" section under "Site details"
- Enter desired name and hit "Save".

For example, for this example I chose: <https://cicd-example-site.netlify.app/>

## 10. OPTIONAL: Custom domain.

If you want a custom domain name you need to go to the site overview, setup your custom DNS, 
and then also continue to setup TLS certificate generation.

We won't cover that here, refer to the Netlify docs.

## 11. Setup the CI/CD configuration at GitLab.

GitLab uses a special file named. `gitlab-ci.yml` at the top of your repository to configure CI/CD.

For the purpose of getting started, this one is as simple as I could make it for our task.
It deploys directly to your site! Netlify also supports [deploying a preview](https://docs.netlify.com/site-deploys/deploy-previews/) first, so I recommend that as an improvement.

Create the .gitlab-ci.yml file, either in GitLab's online editor OR by adding it in a Git commit and pushing it.

```YAML
image: node:18-alpine
 
variables:
  NETLIFY_SITE_ID: $NETLIFY_SITE_ID
  NETLIFY_AUTH_TOKEN: $NETLIFY_AUTH_TOKEN

stages:
  - build

cache:
  paths:
    - .npm/
  key:
    files:
      - package-lock.json

before_script:
  - node --version # version for debugging
  - npm --version # version for debugging
  - npm install netlify-cli --save-dev

build-job:
  stage: build
  script:
    - npx netlify deploy --site $NETLIFY_SITE_ID --auth $NETLIFY_AUTH_TOKEN --prod --dir content/
  only:
    - master
```

You can learn more about GitLab CI/CD  at <https://docs.gitlab.com/ee/ci/>.

## 12.  Test deployment.

- Make a change to something visible on your site content and push it to your GitLab repo.
- Go to your GitLab project. There may be a header showing the running Job.
- If not click "Build" in the left navigation area, then "Jobs" then look for the most recent one at the top, which is probably still running.
- If there are any errors, click on the job, then find the logs and troubleshoot the issue, and retry.
- Verify your site updates show up at your new domain!

Now you are finished. You have a statically hosted site that deploys automatically.

## What Now?

- Check out <https://docs.netlify.com/site-deploys/overview/> to learn more about Netlify's deployment features.
- Check out <https://docs.gitlab.com/ee/ci/> for more information about CI/CD in GitLab.
- Try out a [static-site generator](https://www.cloudflare.com/learning/performance/static-site-generator/) such as Jekyll or Hugo. Netflix has integrations for a number of these which you can learn more about at <https://docs.netlify.com/integrations/frameworks/>. A couple popular options are [Jekyll](https://jekyllrb.com/) and [Hugo](https://gohugo.io/).
  - You can find an extensive list of popular ones at <https://jamstack.org/generators/>
- Get your own domain! You can do this [through Netlify](https://docs.netlify.com/domains-https/custom-domains/) or bring your own from another registrar. Whether you register via Netlify or elsewhere you can use Netlify to host the DNS for your domain, or use external DNS hosting.

I hope you found this useful, and good luck with your site, whether it's an app, business website, project, or just a personal page.

You can find me online at [ranton.org](http://ranton.org).
